
// 统计任务数：
/*  
	col 为列的id的值
	如 counter("a"); 
*/

function counter(col){
	var divs = $("#" + col + " .task");
	var p = $("#" + col + " p");

	var message = "";
	if(divs.length == 0){
		message = "还没有任务哦~";
	} else {
		var message = "一共有";
		message += divs.length;
		message += "项任务哦~";
	}
	p.text(message);
}


// 添加新任务：
/*
	col 为列的id的值
	title 为题目的内容，默认为form的第一个text内容
	time 为截止时间的内容，默认为form第二个text内容
	text 为描述的内容，默认为textarea内容
	顺序：判断是否为空 - 在页面显示 - 更新数据库 - 重新统计任务数 - 提示成功 - 清空表单
	如 onclick="addNewTask('a')；
*/

function addNewTask(col,title = $(".inputer:eq(0)").val(),time = $(".inputer:eq(1)").val(),text = $(".inputer:eq(2)").val(),id = Date.now()){
	var position = $("#" + col + " .tasks");
	var message = "";
	// var title = $(".inputer:eq(0)").val();
	// var time = $(".inputer:eq(1)").val();
	// var text = $(".inputer:eq(2)").val();
	// var id = Date.now();
	// 如果任意一个为空：
	if(title == "" || time == "" || text == ""){
		alert("需要填写完整哦~");
		return false;
	}

	var titleMessage = '<div class="title items">' + title + '</div>';
	var timeMessage = '<div class="time items">' + time + '</div>';
	var textMessage = '<div class="text items">' + text + '</div>';
	// message += '<div class="doing-task task" id="';
	message += '<div class="task" id="';
	message += id + '">';
	message += titleMessage;
	message += timeMessage;
	message += textMessage;
	// message += '<div class="buttonArea">';
	message += '<img class="trash interact" title="删除" src="../img/trashcan.png">';
	message += '<img class="next interact" title="下一阶段" src="../img/next.png">'
	// message += '<img class="previous interact" title="上一阶段" src="../img/previous.png">';
	message += '</div>';
	// message += '</div>';
	position.append(message);
	refreshDatabase();
	counter(col);
	alert("添加成功啦啦啦~~~");
	document.getElementById("add").reset();
}


// 删除任务：
/*
	taskId 是 task的id值，用于获取task以删除
	colId 是 列的id值，用于重新统计该列的任务数
	如 deleteTask("#task-6","c");
*/

function deleteTask(taskId,colId,r = confirm("真的要删除吗？")){
	if(r){
		var task = $("#" + taskId );
		task.remove();
		counter(colId);
		refreshDatabase();
	}
}


// 将任务移至下一个阶段
/*
	taskId 是 task的id值，用于获取task以删除
	colId 是 列的id值，用于重新统计该列的任务数
	如 nextStage("#task-6","c");
*/

function nextStage(taskId,colId,r = confirm("移动到下一个阶段？")){
	if(r){
		if(colId == "a"){
			$("#b .tasks").append($("#" + taskId));
			$("#" + taskId + " .next").after('<img class="previous interact" title="上一阶段" src="../img/previous.png">')
			counter("a");
			counter("b");
		} else if(colId == "b"){
			$("#" + taskId + " .next").remove();
			$("#c .tasks").append($("#" + taskId));
			counter("b");
			counter("c");
		} else{
			alert("error!");
		}

		refreshDatabase();
	}
}



// 将任务移至上一个阶段
/*
	taskId 是 task的id值，用于获取task以删除
	colId 是 列的id值，用于重新统计该列的任务数
	如 nextStage("#task-6","c");
*/
function previousStage(taskId,colId,r = confirm("移动到上一个阶段？")){
	if(r){
		if(colId == "b"){
			$("#" + taskId + " .previous").remove();
			$("#a .tasks").append($("#" + taskId));
			counter("a");
			counter("b");
		} else if(colId == "c"){
			$("#b .tasks").append($("#" + taskId));
			$("#" + taskId + " .previous").before('<img class="next interact" title="下一阶段" src="../img/next.png">')
			counter("b");
			counter("c");
		} else{
			alert("error!");
		}

		refreshDatabase();
	}
}


// 清空全部任务：
/*
	获取全部 class=".task" 的元素，即所有任务，remove掉，
	然后更新任务数统计
*/

function clearAll(){
	var r = confirm("真的要全部删除吗？");
	if(r == true){
		$(".task").remove();
		counter("a");
		counter("b");
		counter("c");
	}
	refreshDatabase();
}


// 更新数据库：
function refreshDatabase(task){
	// alert("更新数据库");
	var tasks = document.getElementsByClassName("task"),
		data = [],
		i = 0;

	$(".task").each(function(){
		var task = $(this);
		var col,title,time,text,idName;

		col = task.parent().parent().attr("id");
		title = task.children(".title").text();
		time = task.children(".time").text();
		text = task.children(".text").text();
		idName = task.attr("id");

		data[i] = [col,title,time,text,idName]
		i += 1
	})
	// alert(data)
	localStorage.setItem("note",JSON.stringify(data));
	// alert("更新完成");
}

// 初始化任务：
function initTasks(){
	var list = JSON.parse(localStorage.getItem("note"));
	if(list){
		// alert(list[1])
		for(var i=0;i<list.length;i++){
			var colNum = list[i][0],
				title = list[i][1],
				time = list[i][2],
				text = list[i][3],
				idName = list[i][4];
				
			var position = $("#" + colNum + " .tasks")
			var titleMessage = '<div class="title items">' + title + '</div>';
			var timeMessage = '<div class="time items">' + time + '</div>';
			var textMessage = '<div class="text items">' + text + '</div>';
			
			var message = '<div class="task" id="';
			message += idName + '">';
			message += titleMessage;
			message += timeMessage;
			message += textMessage;
			// message += '<div class="buttonArea">';
			message += '<img class="trash interact" title="删除" src="../img/trashcan.png">';
			if(colNum == 'a'){
				message += '<img class="next interact" title="下一阶段" src="../img/next.png">'
			} else if(colNum == 'c'){
				message += '<img class="previous interact" title="上一阶段" src="../img/previous.png">';
			} else{
				message += '<img class="next interact" title="下一阶段" src="../img/next.png">'
				message += '<img class="previous interact" title="上一阶段" src="../img/previous.png">';
			}
			
			// message += '</div>';
			message += '</div>';

			position.append(message)
			// alert("fine")
		}
	}
}

// $(function(){initTasks()})

